(function() {
    'use strict';

    angular
        .module('app.layout')
        .controller('Shell', Shell);

    Shell.$inject = ['$timeout', 'config', 'logger', '$auth', '$location', '$window'];

    function Shell($timeout, config, logger, $auth, $location, $window) {
        /*jshint validthis: true */
        var vm = this;

        vm.title = config.appTitle;
        vm.busyMessage = 'Please wait ...';
        vm.isBusy = true;
        vm.showSplash = true;
        vm.authenticated = $auth.isAuthenticated();
        vm.login = login;

        activate();

        

        function activate() {
            logger.success(config.appTitle + ' Chargé!', null);
//            Using a resolver on all routes or dataservice.ready in every controller
//            dataservice.ready().then(function(){
//                hideSplash();
//            });
            hideSplash();
        }

        function hideSplash() {
            //Force a 1 second delay so we can see the splash.
            $timeout(function() {
                vm.showSplash = false;
            }, 1000);
        }
        
          function login(){
                $auth.login(vm.user)
        .then(function() {
          toastr.success('Connexion réussi !');
          $window.location.reload();
        })
        .catch(function(error) {
          toastr.error(error.data.message, error.status);
        });
        }
    }
})();
