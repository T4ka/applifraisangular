(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('dataservice', dataservice);

    /* @ngInject */
    function dataservice($http, $location, $q, exception, logger) {
        var isPrimed = false;
        var primePromise;

        var service = {
            getFraisForfait: getFraisForfait,
            getFraisHorsForfait: getFraisHorsForfait,
            getVisiteur: getVisiteur,
            getMois: getMois,
            getMoisVisiteur: getMoisVisiteur,
            getFraisForfaitVisiteur: getFraisForfaitVisiteur,
            getProfile: getProfile,
            getStatistiques: getStatistiques,
            getFiche: getFiche,
            getEtat: getEtat,
            updateEtat: updateEtat,
            ready: ready,
            addFrais: addFrais,
            addFraisHF: addFraisHF,
            deleteFrais: deleteFrais,
            getTotalHf: getTotalHf,
            updateFraisFF: updateFraisFF,
            updateFraisHF: updateFraisHF,
            deleteFraisFF: deleteFraisFF,
            deleteFraisHF: deleteFraisHF
        };

        return service;

        function deleteFraisFF(id) {
            return $http.delete('/api/deleteFraisFF/' + id)
                .then(get)
                .catch(fail);

            function get(data, status, headers, config) {
                return data.data;
            }

            function fail() {
                return exception.catcher('catch')();
            }
        }

        function deleteFraisHF(id) {
            return $http.delete('/api/deleteFraisHF/' + id)
                .then(get)
                .catch(fail);

            function get(data, status, headers, config) {
                return data.data;
            }

            function fail() {
                return exception.catcher('catch')();
            }
        }


        function updateFraisHF(body, id) {
            return $http.put('/api/updateFraisHF/' + id, body)
                .then(get)
                .catch(fail);

            function get(data, status, headers, config) {
                return data.data;
            }

            function fail() {
                return exception.catcher('catch')();
            }
        }

        function updateFraisFF(body, id) {
            return $http.put('/api/updateFraisFF/' + id, body)
                .then(get)
                .catch(fail);

            function get(data, status, headers, config) {
                return data.data;
            }

            function fail() {
                return exception.catcher('catch')();
            }
        }

        function getTotalHf(id) {
            return $http.get('/api/totalhf/' + id)
                .then(get)
                .catch(fail);

            function get(data, status, headers, config) {
                return data.data;
            }

            function fail() {
                return exception.catcher('catch')();
            }
        }

        function addFraisHF(body) {
            return $http.post('/api/addfraishf', body)
                .then(get)
                .catch(fail);

            function get(data, status, headers, config) {
                return data.data;
            }

            function fail() {
                return exception.catcher('catch')();
            }
        }

        function deleteFrais(body) {
            return $http.put('/api/deletefrais', body)
                .then(get)
                .catch(fail);

            function get(data, status, headers, config) {
                return data.data;
            }

            function fail() {
                return exception.catcher('catch')();
            }
        }

        function addFrais(body) {
            return $http.post('/api/addfrais', body)
                .then(get)
                .catch(fail);

            function get(data, status, headers, config) {
                return data.data;
            }

            function fail() {
                return exception.catcher('catch')();
            }
        }

        function updateEtat(body) {
            return $http.put('/api/updateEtat', body)
                .then(get)
                .catch(fail);

            function get(data, status, headers, config) {
                return data.data;
            }

            function fail() {
                return exception.catcher('catch')();
            }
        }

        function getEtat() {
            return $http.get('/api/etat')
                .then(get)
                .catch(fail);

            function get(data, status, headers, config) {
                return data.data;
            }

            function fail() {
                return exception.catcher('catch')();
            }
        }

        function getFiche(id, mois) {
            return $http.get('/api/fiche/' + id + '/' + mois)
                .then(get)
                .catch(fail);

            function get(data, status, headers, config) {
                return data.data;
            }

            function fail() {
                return exception.catcher('catch')();
            }
        }

        function getStatistiques(id, lib) {
            return $http.get('/api/stat/' + id + '/' + lib)
                .then(get)
                .catch(fail);

            function get(data, status, headers, config) {
                return data.data;
            }

            function fail() {
                return exception.catcher('catch')();
            }
        }

        function getProfile() {
            return $http.get('/api/me')
                .then(get)
                .catch(fail);

            function get(data, status, headers, config) {
                return data.data;
            }

            function fail() {
                return exception.catcher('catch')();
            }
        }

        function getFraisForfait(id, mois) {
            return $http.get('/api/fraisforfait/' + id + '/' + mois)
                .then(get)
                .catch(fail);

            function get(data, status, headers, config) {
                return data.data;
            }

            function fail() {
                return exception.catcher('catch')(message);
            }
        }

        function getFraisHorsForfait(id, mois) {
            return $http.get('/api/fraishorsforfait/' + id + '/' + mois)
                .then(get)
                .catch(fail);

            function get(data, status, headers, config) {
                return data.data;
            }

            function fail() {
                return exception.catcher('catch')(message);
            }
        }

        function getVisiteur() {
            return $http.get('/api/visiteur')
                .then(get)
                .catch(fail);

            function get(data, status, headers, config) {
                return data.data;
            }

            function fail() {
                return exception.catcher('catch')(message);
            }
        }

        function getMois() {
            return $http.get('/api/mois')
                .then(get)
                .catch(fail);

            function get(data, status, headers, config) {
                return data.data;
            }

            function fail() {
                return exception.catcher('catch')(message);
            }
        }

        function getMoisVisiteur(visiteur) {
            return $http.get('/api/moisvisiteur/' + visiteur)
                .then(get)
                .catch(fail);

            function get(data, status, headers, config) {
                return data.data;
            }

            function fail() {
                return exception.catcher('catch')(message);
            }
        }

        function getFraisForfaitVisiteur(id, mois) {
            return $http.get('/api/fraisforfaitvisiteur/' + id + '/' + mois)
                .then(get)
                .catch(fail);

            function get(data, status, headers, config) {
                return data.data;
            }

            function fail() {
                return exception.catcher('catch')(message);
            }
        }



        function prime() {
            // This function can only be called once.
            if (primePromise) {
                return primePromise;
            }

            primePromise = $q.when(true).then(success);
            return primePromise;

            function success() {
                isPrimed = true;
                //logger.info('Primed data');
            }
        }

        function ready(nextPromises) {
            var readyPromise = primePromise || prime();

            return readyPromise
                .then(function () {
                    return $q.all(nextPromises);
                })
                .catch(exception.catcher('"ready" function failed'));
        }

    }
})();