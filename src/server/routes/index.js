module.exports = function (app) {
    var mysql = require('mysql');
    var jwt = require('jwt-simple');
    var moment = require('moment');

    var pool = mysql.createPool({
        connectionLimit: 100,
        multipleStatements: true,
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'fcommonbdd'
    });


    /*
     |--------------------------------------------------------------------------
     | Routes Applifrais
     |--------------------------------------------------------------------------
     */

    app.get('/api/fraisforfait/:id/:mois', getFraisForfait); //done
    app.get('/api/fraishorsforfait/:id/:mois', getFraisHorsForfait); //done
    app.get('/api/visiteur', getVisiteur); //done
    app.get('/api/mois', getMois); //done
    app.get('/api/moisvisiteur/:id', getMoisVisiteur); //done
    app.get('/api/fraisforfaitvisiteur/:id/:mois', getFraisForfaitVisiteur); //non valide
    app.get('/api/stat/:id/:lib', getStatistiques); //done
    app.get('/api/fiche/:id/:mois', getFiche); //done
    app.get('/api/etat', getEtat); //done
    app.get('/api/totalhf/:id', getTotalHf); //done
    app.put('/api/updateEtat', updateEtat); //vide
    app.post('/auth/login', login); //done
    app.get('/api/me', ensureAuthenticated, getMe); //done
    app.post('/api/addfrais', addFrais);
    app.post('/api/addfraishf', addFraisHF);
    app.put('/api/deletefrais', deleteFrais);
    app.put('/api/updateFraisFF/:id', updateFraisFF); //vide
    app.put('/api/updateFraisHF/:id', updateFraisHF); //vide
    app.delete('/api/deleteFraisFF/:id', deleteFraisFF); //vide
    app.delete('/api/deleteFraisHF/:id', deleteFraisHF); //vide

    /*
     |--------------------------------------------------------------------------
     | Routes SuiviA
     |--------------------------------------------------------------------------
     */

    app.get('/api/visite', getvisite); //done
    app.get('/api/visite/:id', getvisiteID); //done
    app.post('/api/visite', postvisite); //done
    app.delete('/api/visite/:id', supprimervisite); //done


    app.get('/api/visiteur', getVisiteurs); //done

    app.get('/api/cabinet', getcabinet);
    app.post('/api/cabinet', postcabinet); //done
    app.put('/api/cabinet', putcabinet); //done

    app.get('/api/medecin', getmedecin); //done
    app.post('/api/medecin', postmedecin); //done
    app.put('/api/medecin', putmedecin); //done

    app.put('/api/affectvisiteur/:idvisiteur', affectVisiteur);

    app.get('/api/timediff/:id', getTimeDiff);
    app.get('/api/totalwait/:id', getTotalWait);
    app.get('/api/totaltimejour/:id/:date', getTotaltimejour);
    app.get('/api/NbrVisiteparJour/:id/:date', getNbrVisiteparJour);

    /*
     |--------------------------------------------------------------------------
     | Routes SuiviA
     |--------------------------------------------------------------------------
     */


    app.get('/api/visite', getVisites);
    app.post('/api/visite', postVisites);
    app.get('/api/medecin', getMedecins);
    app.get('/api/medecin/:id', getMedecinById);
    app.get('/api/cabinet', getCabinets);
    app.get('/api/cabinet/:id', getCabinetById);


    /*
|-------------------------------------------------------------------------------------------------------
| Fonctions SuiviAA
|-------------------------------------------------------------------------------------------------------
*/
    function getVisites(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('select * from visite', function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();

            });
        });
    }

    function getTotalHf(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('select sum(montant) as total from lignefraishorsforfait where idVisiteur = ?', [req.params.id], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();

            });
        });
    }

    function postVisites(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('insert into visite set ?', [req.body], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();

            });
        });
    }

    function getMedecins(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('select * from medecin', function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();

            });
        });
    }

    function getMedecinById(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('select * from medecin where idMedecin = ?', [req.params.id], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();

            });
        });
    }

    function getCabinets(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('select * from cabinet', function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();

            });
        });
    }

    function getCabinetById(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('select * from cabinet where idCabinet = ?', [req.params.id], function (error, results, fields) {
                if (error) throw error;

                res.send(results); //t
                connection.release();

            });
        });
    }



    /*
|-------------------------------------------------------------------------------------------------------
| Fonctions SuiviA
|-------------------------------------------------------------------------------------------------------
*/


    /*
     |--------------------------------------------------------------------------
     | Fonctions statistiques (R)
     |--------------------------------------------------------------------------
     */


    function affectVisiteur(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('update medecin set ?, idVisiteur = ? where idMedecin = ?', [req.body, req.params.idvisiteur, req.body.idMedecin], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();
            });
        });
    }



    function getTimeDiff(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('select distinct visite.idVisite, visite.dateV, TIMEDIFF(heure_depart,heure_arrive) as timediff from visite, medecin, visiteur where visite.idmedecin = medecin.idmedecin and medecin.idvisiteur = visiteur.id and medecin.idvisiteur = ? order by visite.idvisite', [req.params.id], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                console.log(results);
                connection.release();
            });
        });
    }

    function getTotalWait(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('select sec_to_time(sum(unix_timestamp(heure_depart) - unix_timestamp(heure_arrive))) as sumtime from visite, medecin, visiteur where visite.idmedecin = medecin.idmedecin and medecin.idvisiteur = visiteur.id and medecin.idvisiteur = ? order by  visite.idvisite', [req.params.id], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                console.log(results);
                connection.release();

            });
        });
    }


    function getTotaltimejour(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('select sec_to_time(sum(unix_timestamp(heure_depart) - unix_timestamp(heure_arrive))) as sumtime  from visite, medecin, visiteur where visite.idmedecin = medecin.idmedecin and medecin.idvisiteur = visiteur.id and medecin.idvisiteur = ? and visite.datev = ? order by visite.idvisite', [req.params.id, req.params.date], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                console.log(results);
                connection.release();
            });
        });
    }


    function getNbrVisiteparJour(req, res, next) {
        pool.getConnection(function (err, connection) {

            connection.query('select COUNT(visite.idvisite) as sumtime from visite, medecin, visiteur where  visite.idmedecin = medecin.idmedecin and medecin.idvisiteur = visiteur.id and medecin.idvisiteur = ? and visite.datev = ? order by visite.idvisite', [req.params.id, req.params.date], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                console.log(results);
                connection.release();
            });
        });
    }



    /*
     |--------------------------------------------------------------------------
     | Fonctions visiteur (R)
     |--------------------------------------------------------------------------
     */

    function getVisiteurs(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('select * from visiteur', function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                console.log(results);
                connection.release();

            });
        });
    }

    //Inscription enlevée


    /*
    |--------------------------------------------------------------------------
    | Fonctions visite (CRD)
    |--------------------------------------------------------------------------
    */

    function postvisite(req, res, next) {
        console.log(req.body);
        pool.getConnection(function (err, connection) {
            connection.query('insert into visite set ?', [req.body], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();
            });
        });
    }

    function getvisite(req, res, next) {
        console.log(req.body);
        pool.getConnection(function (err, connection) {
            connection.query('select * from visite', function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                console.log(results);
                connection.release();
            });
        });
    }

    function getvisiteID(req, res, next) {
        console.log("here", req.params.id);
        pool.getConnection(function (err, connection) {
            connection.query('select visite.idVisite, visite.dateV, visite.rdv, visite.heure_arrive,visite.heure_depart,visite.idmedecin FROM visite JOIN medecin ON visite.idmedecin = medecin.idMedecin JOIN visiteur ON medecin.idVisiteur = visiteur.id WHERE medecin.idVisiteur = ? ORDER BY visite.dateV desc', [req.params.id], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                console.log(results);
                connection.release();
            });
        });
    }



    function supprimervisite(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('delete from visite where idVisite = ?', [req.params.id], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                console.log(results);
                connection.release();
            });
        });
    }


    /*
    |--------------------------------------------------------------------------
    | Fonctions medecin (CRU)
    |--------------------------------------------------------------------------
    */


    function postmedecin(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('insert into medecin set ?', [req.body], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();
            });
        });
    }


    function getmedecin(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('select * from medecin', function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                console.log(results);
                connection.release();
            });
        });
    }

    function putmedecin(req, res, next) {
        pool.getConnection(function (err, connection) {

            connection.query('update medecin set ? where idMedecin = ?', [req.body, req.body.idMedecin], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();
            });
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Fonctions cabinet (CRU)
    |--------------------------------------------------------------------------
    */

    function postcabinet(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('insert into cabinet set ?', [req.body], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();
            });
        });
    }

    function getcabinet(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('select * from cabinet', function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                console.log(results);
                connection.release();
            });
        });
    }



    function putcabinet(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('update cabinet set ? where idCabinet = ?', [req.body, req.body.idCabinet], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();
            });
        });
    }




















    /*
|-------------------------------------------------------------------------------------------------------
| Fonctions Applifrais
|-------------------------------------------------------------------------------------------------------
*/

    function deleteFraisFF(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('delete from lignefraisforfait where IDFF = ?', [req.params.id], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();

            });
        });
    }

    function deleteFraisHF(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('delete from lignefraishorsforfait where id = ?', [req.params.id], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();

            });
        });
    }


    function updateFraisHF(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('update lignefraishorsforfait set ? where id = ?', [req.body, req.params.id], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();

            });
        });
    }


    function updateFraisFF(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('update lignefraisforfait set ? where IDFF = ?', [req.body, req.params.id], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();

            });
        });
    }


    function deleteFrais(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('delete from fichefrais where idVisiteur = ? and mois = ? and not exists(select * from lignefraisforfait where idVisiteur = fichefrais.idVisiteur and mois = fichefrais.mois) and not exists(select * from lignefraishorsforfait where idVisiteur = fichefrais.idVisiteur and mois = fichefrais.mois)', [req.body.idVisiteur, req.body.mois], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                console.log(results);
                connection.release();

            });
        });


    }

    function addFrais(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('insert into fichefrais(idVisiteur, mois, nbJustificatifs, montantValide, dateModif, idEtat) values(?,?,1,?,NOW(),?) on duplicate key update montantValide = ?, dateModif = NOW(); insert into lignefraisforfait(idVisiteur, mois, idFraisForfait, quantite, description) values(?,?,?,?,?)', [req.body.idVisiteur, req.body.mois, req.body.total, "CR", req.body.total, req.body.idVisiteur, req.body.mois, req.body.idFraisForfait, req.body.quantite, req.body.description], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                console.log(results);
                connection.release();

            });
        });
    }

    function addFraisHF(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('insert into fichefrais(idVisiteur, mois, nbJustificatifs, montantValide, dateModif, idEtat) values(?,?,1,?,NOW(),?) on duplicate key update montantValide = ?, dateModif = NOW(); insert into lignefraishorsforfait(idVisiteur, mois, libelle, date, montant) values(?,?,?,?,?)', [req.body.idVisiteur, req.body.mois, req.body.total, "CR", req.body.total, req.body.idVisiteur, req.body.mois, req.body.libelle, req.body.date, req.body.montant], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                console.log(results);
                connection.release();

            });
        });
    }

    function getEtat(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('select * from etat', function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                console.log(results);
                connection.release();

            });
        });
    }


    function updateEtat(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('update fichefrais set ? where idVisiteur = ? and mois = ?', [req.body, req.body.idVisiteur, req.body.mois], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();

            });
        });
    }




    function getFiche(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('select * from fichefrais as f join etat as e on f.idEtat = e.id where f.idVisiteur = ? and f.mois = ?', [req.params.id, req.params.mois], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();

            });
        });
    }

    function getStatistiques(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('select *, SUM(t1.quantite) as totalquantite, SUM(t1.quantite * t2.montant) as total from lignefraisforfait as t1, fraisforfait as t2 where t1.idFraisForfait = t2.id and t1.idVisiteur = ? and t1.idFraisForfait = ?', [req.params.id, req.params.lib], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();

            });
        });
    }

    function getFraisForfait(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('select * from lignefraisforfait where idVisiteur = ? and mois = ?', [req.params.id, req.params.mois], function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();

            });
        });
    }

    function getFraisHorsForfait(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('select * from lignefraishorsforfait where idVisiteur = ? and mois = ? group by date order by date DESC', [req.params.id, req.params.mois], function (error, results, fields) {
                if (error) throw error;
                res.send(results); //test
                connection.release();

            });
        });
    }

    function getVisiteur(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('select * from visiteur as v join fichefrais as f on v.id = f.idVisiteur group by v.id', function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();

            });
        });
    }


    function getMois(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('select distinct mois from fichefrais', function (error, results, fields) {
                if (error) throw error;
                res.send(results);
                connection.release();

            });
        });
    }

    function getMoisVisiteur(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('select distinct mois from fichefrais where idVisiteur = ?', [req.params.id], function (error, results, fields) {
                if (error) throw error;
                console.log(req.params.id);
                res.send(results);
                connection.release();

            });
        });
    }

    function getFraisForfaitVisiteur(req, res, next) {
        pool.getConnection(function (err, connection) {
            connection.query('select * from lignefraisforfait as ligne join fraisforfait as libelle on ligne.idFraisForfait = libelle.id where ligne.idVisiteur = ? and ligne.mois = ?; select * from lignefraishorsforfait as ligne  where ligne.idVisiteur = ? and ligne.mois = ?', [req.params.id, req.params.mois, req.params.id, req.params.mois], function (error, results, fields) {
                if (error) throw error;
                console.log(req.params.mois);
                var allresults = [results[0], results[1]];
                res.send(allresults);
                connection.release();

            });
        });
    }














    /*
     |--------------------------------------------------------------------------
     | Log in with Email
     |--------------------------------------------------------------------------
     */
    function login(req, res) {
        pool.getConnection(function (err, connection) {
            connection.query('select * from visiteur where login = ? and mdp = ?', [req.body.username, req.body.password], function (error, user) {
                if (error) throw error;

                if (user.length == 0) {
                    return res.status(401).send({
                        message: 'Nom de compte ou mot de passe invalide !'
                    });
                } else {
                    res.send({
                        token: createJWT(user[0]),
                        user: user[0]
                    });
                }
                connection.release();

            });
        });

    }


    /*
     |--------------------------------------------------------------------------
     | GET /api/me
     |--------------------------------------------------------------------------
     */
    function getMe(req, res) {


        pool.getConnection(function (err, connection) {
            // Use the connection
            connection.query('select * from visiteur where id = ?', [req.user], function (error, results, fields) {
                if (error) throw error;
                //  console.log(results);

                res.send(results);
                // And done with the connection.
                connection.release();

                // Handle error

                // Don't use the connection here, it has been returned to the pool.
            });
        });

    }


    /*
     |--------------------------------------------------------------------------
     | Login Required Middleware
     |--------------------------------------------------------------------------
     */
    function ensureAuthenticated(req, res, next) {
        if (!req.header('Authorization')) {
            return res.status(401).send({
                message: 'Please make sure your request has an Authorization header'
            });
        }
        var token = req.header('Authorization').split(' ')[1];

        var payload = null;
        try {
            payload = jwt.decode(token, "fqsdg64");
        } catch (err) {
            return res.status(401).send({
                message: err.message
            });
        }

        if (payload.exp <= moment().unix()) {
            return res.status(401).send({
                message: 'Token has expired'
            });
        }
        req.user = payload.sub;
        next();
    }

    /*
     |--------------------------------------------------------------------------
     | Generate JSON Web Token
     |--------------------------------------------------------------------------
     */
    function createJWT(user) {
        var payload = {
            sub: user.id,
            iat: moment().unix(),
            exp: moment().add(14, 'days').unix()
        };
        return jwt.encode(payload, "fqsdg64");
    }


};