(function() {
    'use strict';

    angular
        .module('app.ajouterFrais')
        .run(appRun);

    // appRun.$inject = ['routehelper']

    /* @ngInject */
    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return [
            {
                url: '/',
                config: {
                    templateUrl: 'app/ajouterFrais/ajouterFrais.html',
                    controller: 'AjouterFrais',
                    controllerAs: 'vm',
                    title: 'AjouterFrais',
                    settings: {
                        nav: 2,
                        content: '<i class="fa fa-plus-circle"></i> AjouterFrais'
                    }
                }
            }
        ];
    }
})();
