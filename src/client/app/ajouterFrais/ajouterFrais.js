(function () {
    'use strict';

    angular
        .module('app.ajouterFrais')
        .controller('AjouterFrais', AjouterFrais);

    /* @ngInject */
    function AjouterFrais(dataservice, logger, $filter, $auth, $rootScope, $location) {
        /*jshint validthis: true */
        var vm = this;
        vm.fraisforfait = [];
        vm.fraishorsforfait = [];
        vm.fiche = [];
        vm.title = 'Ajouter des frais';
        vm.currentMonth = $filter('date')(Date.now(), 'yyyyMM');
        vm.translateInMonth = translateInMonth;
        vm.stat = [];
        vm.libelle = ['REP', 'NUI', 'KM', 'ETP'];
        vm.finalstat = [];
        vm.addFrais = addFrais;
        vm.totalstat = 0;
        vm.totalhf = 0;
        vm.updateFraisFF = updateFraisFF;
        vm.updateFraisHF = updateFraisHF;
        vm.deleteFraisFF = deleteFraisFF;
        vm.deleteFraisHF = deleteFraisHF;
        var d = new Date();
        var date = d.getFullYear() + ("0" + (d.getMonth() + 1)).slice(-2);


        function deleteFraisFF(id) {
            return dataservice.deleteFraisFF(id).then(function (data) {
                logger.success("Frais Supprimé");
                return activate();
            });
        }

        function deleteFraisHF(id) {
            return dataservice.deleteFraisHF(id).then(function (data) {
                logger.success("Frais Supprimé");
                    return activate();
            });
        }

        function updateFraisFF(form, id) {
            var form = {
                idFraisForfait : form.idFraisForfait,
                description : form.description,
                quantite : form.quantite
            }
            return dataservice.updateFraisFF(form, id).then(function (data) {
                logger.success("Frais Modifié");
                    return activate();
            });
        }

        function updateFraisHF(form) {
            return dataservice.updateFraisHF(form).then(function (data) {
                logger.success("Frais Modifié");
                    return activate();
            });
        }

        function addFrais(form) {
            if (form.select == 'Autre') {
                var body = {
                    idVisiteur: $rootScope.user.id,
                    mois: date,
                    libelle: form.libelle,
                    date: form.date,
                    total: vm.totalhf,
                    montant: form.montant

                }

                return dataservice.deleteFrais(body).then(function (data) {
                    return dataservice.addFraisHF(body).then(function (data) {
                        logger.success("Frais hors forfait ajoutée avec succès !");
                        return activate();
                    })
                })

            } else {

                var body = {
                    idVisiteur: $rootScope.user.id,
                    mois: date,
                    idFraisForfait: form.select,
                    quantite: form.quantite,
                    total: vm.totalstat,
                    description: form.description

                }


                return dataservice.deleteFrais(body).then(function (data) {
                    return dataservice.addFrais(body).then(function (data) {
                        logger.success("Frais forfait ajoutée avec succès !");
                        return activate();
                    })
                })

            }


        }


        //  $auth.logout();

        activate();

        function activate() {
            if ($rootScope.user.level == 1) $location.path('/comptable');
            var promises = [getFraisForfait(), getFraisHorsForfait(), getStatistiques(), getFiche(), getTotalHf()];
            return dataservice.ready(promises).then(function () {
                //   logger.info('Frais chargés');
            })
        }

        function getFiche() {
            return dataservice.getFiche($rootScope.user.id, date).then(function (data) {
                vm.fiche = data[0];
                console.log(vm.fiche);
                return vm.fiche;
            });
        }

        function getTotalHf() {
            return dataservice.getTotalHf($rootScope.user.id).then(function (data) {
                vm.totalhf = data[0].total;
                console.log(vm.totalhf);
                return vm.totalhf;
            });
        }




        function getStatistiques() {
            if (vm.finalstat.length > 0) vm.finalstat = [];
            vm.libelle.forEach(function (element) {
                return dataservice.getStatistiques($rootScope.user.id, element).then(function (data) {

                    //   if(vm.finalstat.length  < 1)
                    vm.finalstat.push(data);
                    data.forEach(function (total) {
                        vm.totalstat += total.total;
                    })

                });
            });
            return vm.finalstat;

        }


        function getFraisForfait() {
            return dataservice.getFraisForfait($rootScope.user.id, date).then(function (data) {
                vm.fraisforfait = data;
                return vm.fraisforfait;
            });
        }

        function getFraisHorsForfait() {
            return dataservice.getFraisHorsForfait($rootScope.user.id, date).then(function (data) {
                vm.fraishorsforfait = data;
                return vm.fraishorsforfait;
            });
        }

        function translateInMonth(month) {

            switch (month) {
                case "01":
                    return "Janvier";
                case "02":
                    return "Février";
                case "03":
                    return "Mars";
                case "04":
                    return "Avril";
                case "05":
                    return "Mai";
                case "06":
                    return "Juin";
                case "07":
                    return "Juillet";
                case "08":
                    return "Août";
                case "09":
                    return "Septembre";
                case "10":
                    return "Octobre";
                case "11":
                    return "Novembre";
                case "12":
                    return "Décembre";
            }
        }

    }
})();