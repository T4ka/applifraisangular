(function () {
    'use strict';

    angular
        .module('app.comptable')
        .controller('Comptable', Comptable);

    /* @ngInject */
    function Comptable(dataservice, logger, $location, $rootScope) {
        /*jshint validthis: true */
        //Variables
        var vm = this;
        vm.comptable = [];
        vm.visiteur = [];
        vm.moisVisiteur = [];
        vm.fraisForfaitVisiteur = [];
        vm.fraisHorsForfaitVisiteur = [];
        vm.fiche = [];
        vm.etat = [];
        vm.title = 'Gestion de frais Comptable';

        vm.updateFraisFF = updateFraisFF;
        vm.updateFraisHF = updateFraisHF;
        vm.deleteFraisFF = deleteFraisFF;
        vm.deleteFraisHF = deleteFraisHF;

        //Fonctions
        vm.translateInMonth = translateInMonth;
        vm.translateIdVisiteur = translateIdVisiteur;
        vm.selectVisiteur = selectVisiteur;
        vm.selectMois = selectMois;
        vm.updateEtat = updateEtat


        activate();

        function activate() {

            if ($rootScope.user.level == 0) $location.path('/');
            var promises = [getVisiteur(), getEtat()];
            return dataservice.ready(promises).then(function () {
                //  logger.info('Visiteurs chargés');
                //logger.info('Mois chargés');
            })


        }

        function deleteFraisFF(id) {
            return dataservice.deleteFraisFF(id).then(function (data) {
                logger.success("Frais Supprimé");
                return selectMois(vm.choiceMonth);
            });
        }

        function deleteFraisHF(id) {
            return dataservice.deleteFraisHF(id).then(function (data) {
                logger.success("Frais Supprimé");
                return selectMois(vm.choiceMonth);
            });
        }

        function updateFraisFF(form, id) {
            var form = {
                idFraisForfait: form.idFraisForfait,
                description: form.description,
                quantite: form.quantite
            }
            return dataservice.updateFraisFF(form, id).then(function (data) {
                logger.success("Frais Modifié");
                return selectMois(vm.choiceMonth);
            });
        }

        function updateFraisHF(form, id) {
            return dataservice.updateFraisHF(form, id).then(function (data) {
                logger.success("Frais Modifié");
                return selectMois(vm.choiceMonth);
            });
        }


        function getEtat() {
            return dataservice.getEtat().then(function (data) {
                vm.etat = data;
                console.log(vm.etat);
                return vm.etat;
            });
        }


        function getFiche(visiteur, mois) {
            return dataservice.getFiche(visiteur, mois).then(function (data) {
                vm.fiche = data[0];
                console.log(vm.fiche);
                return vm.fiche;
            });
        }

        function getVisiteur() {
            return dataservice.getVisiteur().then(function (data) {
                vm.visiteur = data;
                return vm.visiteur;
            });
        }


        function selectVisiteur(visiteur) {
            return dataservice.getMoisVisiteur(visiteur).then(function (data) {
                vm.moisVisiteur = data;
                console.log(vm.moisVisiteur);
                return vm.moisVisiteur;
            });
        }

        function selectMois(mois) {
            return dataservice.getFraisForfaitVisiteur(vm.choiceVisiteur, mois).then(function (data) {
                vm.fraisForfaitVisiteur = data[0];
                vm.fraisHorsForfaitVisiteur = data[1];
                console.log(vm.fraisForfaitVisiteur);
                return vm.fraisForfaitVisiteur;
            }).then(getFiche(vm.choiceVisiteur, mois));
        }

        function updateEtat(etat) {
            var update = {
                idVisiteur: vm.choiceVisiteur,
                mois: vm.choiceMonth,
                idEtat: etat
            }
            return dataservice.updateEtat(update).then(function (data) {
                logger.success("Fiche modifée avec succès !");
            });
        }


        function translateInMonth(month) {

            switch (month) {
                case "01":
                    return "Janvier";
                case "02":
                    return "Février";
                case "03":
                    return "Mars";
                case "04":
                    return "Avril";
                case "05":
                    return "Mai";
                case "06":
                    return "Juin";
                case "07":
                    return "Juillet";
                case "08":
                    return "Août";
                case "09":
                    return "Septembre";
                case "10":
                    return "Octobre";
                case "11":
                    return "Novembre";
                case "12":
                    return "Décembre";
            }
        }

        function translateIdVisiteur(id) {
            var nom = null;
            vm.visiteur.forEach(function (element) {
                if (element.id === id) {
                    nom = element.nom + ' ' + element.prenom;
                }
            });
            return nom;
        }
    }

})();