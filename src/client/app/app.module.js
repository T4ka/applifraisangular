(function () {
    'use strict';

    angular.module('app', [
        /*
         * Order is not important. Angular makes a
         * pass to register all of the modules listed
         * and then when app.dashboard tries to use app.data,
         * its components are available.
         */

        /*
         * Everybody has access to these.
         * We could place these under every feature area,
         * but this is easier to maintain.
         */
        'app.core',
        'app.widgets',

        /*
         * Feature areas
         */
        'app.ajouterFrais',
        'app.comptable',
        'app.layout'
    ]).run(maiController);

    maiController.$inject = ['$rootScope', 'dataservice', '$auth'];

    function maiController($rootScope, dataservice, $auth) {
        $rootScope.user = [];
        $rootScope.authenticated = $auth.isAuthenticated();

        activate();

        function activate() {

            var promises = [getProfile()];
            return dataservice.ready(promises).then(function () {})


        }

        function getProfile() {
            if(!$auth.isAuthenticated()) return;
            return dataservice.getProfile().then(function (data) {
                $rootScope.user = data[0];
                return $rootScope.user;
            })
        }

    }


})();