(function() {
    'use strict';

    angular
        .module('app.comptable')
        .run(appRun);

    // appRun.$inject = ['routehelper']

    /* @ngInject */
    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return [
            {
                url: '/comptable',
                config: {
                    templateUrl: 'app/comptable/comptable.html',
                    controller: 'Comptable',
                    controllerAs: 'vm',
                    title: 'comptable',
                    settings: {
                        nav: 3,
                        content: '<i class="fa fa-user"></i> Comptable'
                    }
                }
            }
        ];
    }
})();
