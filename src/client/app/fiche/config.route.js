(function() {
    'use strict';

    angular
        .module('app.fiche')
        .run(appRun);

    // appRun.$inject = ['routehelper']

    /* @ngInject */
    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return [
            {
                url: '/fiche',
                config: {
                    templateUrl: 'app/fiche/fiche.html',
                    controller: 'Fiche',
                    controllerAs: 'vm',
                    title: 'fiche',
                    settings: {
                        nav: 3,
                        content: '<i class="fa fa-user"></i> Fiche'
                    }
                }
            }
        ];
    }
})();
